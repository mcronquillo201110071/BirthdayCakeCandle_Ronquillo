def birthdayCakeCandles(n, ar):

    a = max(ar)
    count=0
    for i in range(0,n):
      if a<=ar[i]:
        count+=1
    return count
    
n = int(input().strip())
ar = list(map(int, input().strip().split(' ')))
result = birthdayCakeCandles(n, ar)
print('The answer is: ',result)

# Testing GIT
#practice